// index.js
const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const cors = require('cors')
const crypto = require('crypto')

const app = express()
const PORT = process.env.PORT || 8080

app.use(bodyParser.json())

const corsOptions = {
  origin: ['http://localhost:8000'],
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true
}
app.use(cors(corsOptions))

const users = [
  { id: 1, username: 'macallan', password: bcrypt.hashSync('1234', 10) }
]

const secretKey = crypto.randomBytes(32).toString('hex')
console.log('Secret Key:', secretKey)

const authenticateToken = (req, res, next) => {
  const token = req.header('Authorization')
  if (token == null) return res.sendStatus(401)

  jwt.verify(token, secretKey, (err, user) => {
    if (err) return res.sendStatus(403)
    req.user = user
    next()
  })
}

app.post('/login', (req, res) => {
  const { username, password } = req.body
  // const { callbackUrl } = req.body;
  const user = users.find((u) => u.username === username)

  if (user && bcrypt.compareSync(password, user.password)) {
    const accessToken = generateAccessToken({ username: user.username })
    const refreshToken = generateRefreshToken({ username: user.username })
    res.json({ accessToken, refreshToken })
    // res.status(200).json({ message: 'Login successful', callbackUrl });
  } else {
    res.sendStatus(401)
  }
})

app.get('/user', authenticateToken, (req, res) => {
  const userInfo = {
    id: 1,
    username: 'macallan',
    email: 'anitech222@gmail.com',
    firstName: 'Thanapol',
    lastName: 'Yodpibut'
  }
  res.json(userInfo)
})

app.get('/list', authenticateToken, (req, res) => {
  const userList = [
    { id: 1, username: 'user1', email: 'user1@gmail.com' },
    { id: 2, username: 'user2', email: 'user2@gmail.com' },
    { id: 3, username: 'user3', email: 'user3@gmail.com' }
  ]
  res.json(userList)
})

app.get('/user/:id', authenticateToken, (req, res) => {
  const users = [
    { id: 1, username: 'user1', email: 'user1@gmail.com' },
    { id: 2, username: 'user2', email: 'user2@gmail.com' },
    { id: 3, username: 'user3', email: 'user3@gmail.com' }
  ]
  // ค้นหา user by id
  const user = users.find((user) => user.id === parseInt(req.params.id))

  // ตรวจสอบว่า user  존재
  if (!user) {
    return res.status(404).json({ message: 'ไม่พบผู้ใช้' })
  }

  // ส่งข้อมูล user กลับไป
  res.json(user)
})

app.post('/user', authenticateToken, (req, res) => {
  // จำลองข้อมูล user (แทนที่ด้วยข้อมูลจริงจากฐานข้อมูล)
  const users = [
    { id: 1, username: 'user1', email: 'user1@gmail.com' },
    { id: 2, username: 'user2', email: 'user2@gmail.com' },
    { id: 3, username: 'user3', email: 'user3@gmail.com' }
  ]

  // ดึงข้อมูล body
  const newUser = req.body

  // เพิ่ม id ใหม่
  newUser.id = users.length + 1

  // เพิ่ม user ใหม่ไปยัง array
  users.push(newUser)

  // ส่งข้อมูล user ใหม่ กลับไป
  res.json(newUser)
})

app.put('/user/:id', authenticateToken, (req, res) => {
  // จำลองข้อมูล user (แทนที่ด้วยข้อมูลจริงจากฐานข้อมูล)
  const users = [
    { id: 1, username: 'user1', email: 'user1@gmail.com' },
    { id: 2, username: 'user2', email: 'user2@gmail.com' },
    { id: 3, username: 'user3', email: 'user3@gmail.com' }
  ]

  // ค้นหา user by id
  const userIndex = users.findIndex(
    (user) => user.id === parseInt(req.params.id)
  )

  // ตรวจสอบว่า user  존재
  if (userIndex === -1) {
    return res.status(404).json({ message: 'ไม่พบผู้ใช้' })
  }

  // ดึงข้อมูล body
  const updatedUser = req.body

  // อัปเดตข้อมูล user
  users[userIndex] = { ...users[userIndex], ...updatedUser }

  // ส่งข้อมูล user ที่แก้ไขแล้ว กลับไป
  res.json(users[userIndex])
})

app.delete('/user/:id', authenticateToken, (req, res) => {
  // จำลองข้อมูล user (แทนที่ด้วยข้อมูลจริงจากฐานข้อมูล)
  const users = [
    { id: 1, username: 'user1', email: 'user1@gmail.com' },
    { id: 2, username: 'user2', email: 'user2@gmail.com' },
    { id: 3, username: 'user3', email: 'user3@gmail.com' }
  ]

  // ค้นหา user by id
  const userIndex = users.findIndex(
    (user) => user.id === parseInt(req.params.id)
  )

  // ตรวจสอบว่า user  존재
  if (userIndex === -1) {
    return res.status(404).json({ message: 'ไม่พบผู้ใช้' })
  }

  // จำลองการลบ (ลบ user ออกจาก array)
  users.splice(userIndex, 1)

  // ส่งข้อความ success กลับไป
  res.json({ message: 'ลบผู้ใช้สำเร็จ' })
})

app.get('/list-plan', authenticateToken, (req, res) => {
  const itemPlan = [
    {
      id: 1,
      name: '',
      center: { lat: -31.56391, lng: 147.154312 },
      statusOpen: false,
      img: 'https://img.wongnai.com/p/256x256/2023/05/13/eab4588c1ca648faa8a5788fcd8c6b6d.jpg',
      time: ''
    },
    {
      id: 2,
      name: '',
      center: { lat: -33.718234, lng: 150.363181 },
      statusOpen: false,
      img: 'https://img.wongnai.com/p/256x256/2021/09/09/5ceb46b2f0454e43b30fd07ab1fce67b.jpg',
      time: ''
    },
    {
      id: 3,
      name: '',
      center: { lat: -33.727111, lng: 150.371124 },
      statusOpen: false,
      img: 'https://img.wongnai.com/p/256x256/2018/12/16/5f6f34108b3649819b34908c9b82f299.jpg',
      time: ''
    },
    {
      id: 4,
      name: '',
      center: { lat: -33.848588, lng: 151.209834 },
      statusOpen: false,
      img: 'https://img.wongnai.com/p/256x256/2022/11/17/ad05227347474e098e7f6390d410c1af.jpg',
      time: ''
    },
    {
      id: 5,
      name: '',
      center: { lat: -33.851702, lng: 151.216968 },
      statusOpen: false,
      img: 'https://img.wongnai.com/p/256x256/2019/09/25/5d081e9d4df147c5bd0178ce5cb3d3ca.jpg',
      time: ''
    },
    {
      id: 6,
      name: '',
      center: { lat: -34.671264, lng: 150.863657 },
      statusOpen: false,
      img: 'https://img.wongnai.com/p/256x256/2022/08/07/a08d6ca887ef44ff8faaf90ac810302e.jpg',
      time: ''
    },
    {
      id: 7,
      name: '',
      center: { lat: -35.304724, lng: 148.662905 },
      statusOpen: false,
      img: 'https://img.wongnai.com/p/256x256/2018/10/22/27bb72db88c4404eb4819fcce6ff4ca9.jpg',
      time: ''
    },
    {
      id: 8,
      name: '',
      center: { lat: -37.062834, lng: 142.748489 },
      statusOpen: false,
      img: 'https://img.wongnai.com/p/256x256/2018/11/07/605b202ac4da49688e5f212ae895ae8b.jpg',
      time: ''
    },
    {
      id: 9,
      name: '',
      center: { lat: -36.72236, lng: 142.1772 },
      statusOpen: false,
      img: 'https://t1.blockdit.com/photos/2023/05/6468f598518fcbde56ee815b_800x0xcover_VnzEfwHS.jpg',
      time: ''
    },
    {
      id: 10,
      name: '',
      center: { lat: -37.823873, lng: 140.76271 },
      statusOpen: false,
      img: 'https://img.wongnai.com/p/256x256/2021/06/23/83dcf5c337a64cf7b75467c276e982c3.jpg',
      time: ''
    }
  ]
  res.json(itemPlan)
})

function generateAccessToken(user) {
  return jwt.sign(user, secretKey, { expiresIn: '1h' }) // Expires in 1 minute
}

function generateRefreshToken(user) {
  return jwt.sign(user, secretKey, { expiresIn: '2h' }) // Expires in 2 minutes
}

// Refresh Token Route
app.post('/refresh-token', (req, res) => {
  const refreshToken = req.body.refreshToken
  if (refreshToken == null) return res.sendStatus(401)

  // ตรวจสอบว่า Refresh Token ถูกต้องหรือไม่
  jwt.verify(refreshToken, secretKey, (err, user) => {
    if (err) return res.sendStatus(403)

    // ตรวจสอบว่า Refresh Token มีอายุหรือไม่
    const decodedToken = jwt.decode(refreshToken)
    const currentTimestamp = Math.floor(Date.now() / 1000)
    if (decodedToken.exp <= currentTimestamp) {
      return res.sendStatus(401) // Refresh Token หมดอายุ
    }

    // สร้าง Access Token ใหม่
    const accessToken = generateAccessToken({ username: user.username })
    res.json({ accessToken })
  })
})

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`)
})
