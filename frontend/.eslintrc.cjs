module.exports = {
  root: true,
  extends: ['@nuxt/eslint-config'],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': 'error'
  }
}
