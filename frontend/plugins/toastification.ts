import Toast, { POSITION } from 'vue-toastification'
import 'vue-toastification/dist/index.css'

export default defineNuxtPlugin((nuxtApp) => {
  const options = {
    position: POSITION.BOTTOM_RIGHT,
    timeout: 2000
  }
  nuxtApp.vueApp.use(Toast, options)
})
