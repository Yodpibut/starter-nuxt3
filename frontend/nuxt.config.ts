// https://nuxt.com/docs/api/configuration/nuxt-config
import vuetify, { transformAssetUrls } from 'vite-plugin-vuetify'
export default defineNuxtConfig({
  app: {
    head: {
      htmlAttrs: { lang: 'en' },
      title: 'Starter Nuxt 3',
      charset: 'utf-8',
      meta: [
        { name: 'viewport', content: 'width=device-width, initial-scale=1' }
      ],
      script: []
    }
  },
  devtools: { enabled: false },
  devServer: {
    port: 8000
    // host: '0.0.0.0'
  },
  ssr: false,
  css: ['~/assets/sass/mainStyle.scss'],
  runtimeConfig: {
    public: {
      apiBase: process.env.API_URL,
      googleMapsApiKey: process.env.API_KEY_MAPS,
    }
  },
  typescript: {
    typeCheck: true
  },
  imports: {
    autoImport: true,
    dirs: ['composables/**', 'pages/**']
  },
  build: {
    transpile: ['vuetify', '@googlemaps/js-api-loader']
  },
  modules: [
    '@pinia/nuxt',
    '@nuxtjs/i18n',
    'dayjs-nuxt',
    (_options, nuxt) => {
      nuxt.hooks.hook('vite:extendConfig', (config) => {
        // @ts-expect-error
        config.plugins.push(vuetify({ autoImport: true }))
      })
    }
  ],
  plugins: [],
  pinia: {
    storesDirs: ['./store/**']
  },
  i18n: {
    vueI18n: './store/translate/i18n.config.ts'
  },
  dayjs: {
    locales: ['en', 'th'],
    plugins: ['relativeTime', 'utc', 'timezone', 'buddhistEra'],
    defaultLocale: 'th',
    defaultTimezone: 'Asia/Bangkok'
  },
  vite: {
    vue: {
      template: {
        transformAssetUrls
      }
    }
  }
})
