import en from './locale/en.locale'
import th from './locale/th.locale'
export default defineI18nConfig(() => ({
  legacy: false,
  locale: localStorage.getItem('locale') || 'en',
  messages: {
    en,
    th
  }
}))
