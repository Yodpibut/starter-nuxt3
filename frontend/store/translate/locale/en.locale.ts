import welcome from './welcome/en.welcome'
import fromLogin from './fromLogin/en'
import nav from './nav/en.nav'

export default {
  welcome,
  fromLogin,
  nav,
  langId: 'en',
  langName: 'English'
}
