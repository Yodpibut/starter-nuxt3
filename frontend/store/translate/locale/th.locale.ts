import welcome from './welcome/th.welcome'
import fromLogin from './fromLogin/th'
import nav from './nav/th.nav'

export default {
  welcome,
  fromLogin,
  nav,
  langId: 'th',
  langName: 'ไทย'
}
