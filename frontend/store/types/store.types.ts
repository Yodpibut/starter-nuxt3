// ------------------------ auth ------------------------
export interface IUser {
  id: number
  username: string
  email: string
  firstName: string
  lastName: string
}

export interface ILogin {
  username: string
  password: string
}

export interface IJwtToken {
  accessToken: string
  refreshToken: string
}
// ------------------------ user ------------------------
export interface IUserList {
  [key: string]: any
}
