export const useAuthStore = defineStore('auth', () => {
  const runtimeConfig = useRuntimeConfig()
  const user = ref()
  const accessToken = useCookie('accessToken')
  const refreshToken = useCookie('refreshToken')
  const isAuthenticated = computed(() => !!accessToken.value)
  const useToast = useShowToast()

  async function login(item: ILogin) {
    try {
      const res = await $fetch<IJwtToken>(
        `${runtimeConfig.public.apiBase}/login`,
        {
          method: 'POST',
          body: item
        }
      )
      // console.log(res)
      accessToken.value = res.accessToken
      refreshToken.value = res.refreshToken
    } catch (error) {
      console.log(error)
      useToast('Error logging in', 'error')
    }
  }

  async function getUser() {
    try {
      const res = await $fetch<IUser>(`${runtimeConfig.public.apiBase}/user`, {
        method: 'GET',
        headers: {
          Authorization: `${accessToken.value}`
        }
      })
      // console.log(res)
      user.value = res
    } catch (error: any) {
      console.error('Error refreshing token:', error)
      if (error.response.status === 403) {
        refreshAccessToken()
      } else {
        console.log(error)
      }
    }
  }

  async function refreshAccessToken() {
    try {
      const res = await $fetch<IJwtToken>(
        `${runtimeConfig.public.apiBase}/refresh-token`,
        {
          method: 'POST',
          body: {
            refreshToken: refreshToken.value
          }
        }
      )
      // console.log(res)
      updateAccessToken(res.accessToken)
    } catch (error) {
      console.error('Error refreshing token:', error)
      logout()
    }
  }

  function updateAccessToken(newAccessToken: string) {
    accessToken.value = newAccessToken
  }

  function logout() {
    accessToken.value = ''
    refreshToken.value = ''
    user.value = null
    useRouter().push('/login')
  }

  return {
    accessToken,
    refreshToken,
    isAuthenticated,
    user,
    login,
    getUser,
    logout,
    refreshAccessToken
  }
})
