export const useTodosStore = defineStore('todos', {
  state: () => ({
    todos: []
  }),
  actions: {
    async fetchTodos() {
      const { data }: any = await useFetch(
        'https://jsonplaceholder.typicode.com/todos'
      )
      if (data.value) {
        // console.log(data.value)
        this.todos = data.value
      }
    }
  }
})
