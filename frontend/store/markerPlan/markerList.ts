export const useMarkerListStore = defineStore('markerList', () => {
  const runtimeConfig = useRuntimeConfig()
  const authStore = useAuthStore()
  const loading = useLoadingMain()

  const markerList = ref<IUserList[]>([])

  async function getMarkerList() {
    try {
      loading.value = true
      const res = await $fetch<IUserList[]>(
        `${runtimeConfig.public.apiBase}/list-plan`,
        {
          method: 'GET',
          headers: {
            Authorization: `${authStore.accessToken}`
          }
        }
      )
      console.log(res)
      markerList.value = res
      setTimeout(() => {
        loading.value = false
      }, 2000)
    } catch (error) {
      console.error(error)
      loading.value = false
      return error
    }
  }

  return {
    markerList,
    getMarkerList
  }
})
