export const useUserStore = defineStore('user', () => {
  const runtimeConfig = useRuntimeConfig()
  const authStore = useAuthStore()
  const loading = useLoadingMain()

  const userList = ref<IUserList[]>([])

  async function getUserList() {
    try {
      loading.value = true
      const res = await $fetch<IUserList[]>(
        `${runtimeConfig.public.apiBase}/list`,
        {
          method: 'GET',
          headers: {
            Authorization: `${authStore.accessToken}`
          }
        }
      )
      console.log(res)
      userList.value = res
      setTimeout(() => {
        loading.value = false
      }, 2000)
    } catch (error) {
      console.error(error)
      loading.value = false
      return error
    }
  }

  return {
    userList,
    getUserList
  }
})
