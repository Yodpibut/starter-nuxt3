import { Loader } from '@googlemaps/js-api-loader'
import { MarkerClusterer } from '@googlemaps/markerclusterer'

export function useGoogleMaps() {
  const runtimeConfig = useRuntimeConfig()

  const loader = new Loader({
    apiKey: runtimeConfig.public.googleMapsApiKey,
    version: 'weekly',
    libraries: ['places']
  })

  return {
    loader,
    MarkerClusterer
  }
}

export function useCreateMap(
  element: HTMLElement,
  center: google.maps.LatLng,
  zoom: number
): google.maps.Map {
  return new google.maps.Map(element, {
    center,
    zoom,
    minZoom: zoom,
    disableDefaultUI: true,
    mapTypeControlOptions: {
      mapTypeIds: ['HYBRID']
    },
    styles: [
      {
        elementType: 'geometry',
        stylers: [
          {
            color: '#ebe3cd'
          }
        ]
      },
      {
        elementType: 'labels.text.fill',
        stylers: [
          {
            color: '#523735'
          }
        ]
      },
      {
        elementType: 'labels.text.stroke',
        stylers: [
          {
            color: '#f5f1e6'
          }
        ]
      },
      {
        featureType: 'administrative',
        elementType: 'geometry.stroke',
        stylers: [
          {
            color: '#c9b2a6'
          }
        ]
      },
      {
        featureType: 'administrative.land_parcel',
        elementType: 'geometry.stroke',
        stylers: [
          {
            color: '#dcd2be'
          }
        ]
      },
      {
        featureType: 'administrative.land_parcel',
        elementType: 'labels.text.fill',
        stylers: [
          {
            color: '#ae9e90'
          }
        ]
      },
      {
        featureType: 'landscape.natural',
        elementType: 'geometry',
        stylers: [
          {
            color: '#dfd2ae'
          }
        ]
      },
      {
        featureType: 'poi',
        elementType: 'geometry',
        stylers: [
          {
            color: '#dfd2ae'
          }
        ]
      },
      {
        featureType: 'poi',
        elementType: 'labels.text.fill',
        stylers: [
          {
            color: '#93817c'
          }
        ]
      },
      {
        featureType: 'poi.park',
        elementType: 'geometry.fill',
        stylers: [
          {
            color: '#a5b076'
          }
        ]
      },
      {
        featureType: 'poi.park',
        elementType: 'labels.text.fill',
        stylers: [
          {
            color: '#447530'
          }
        ]
      },
      {
        featureType: 'road',
        elementType: 'geometry',
        stylers: [
          {
            color: '#f5f1e6'
          }
        ]
      },
      {
        featureType: 'road.arterial',
        elementType: 'geometry',
        stylers: [
          {
            color: '#fdfcf8'
          }
        ]
      },
      {
        featureType: 'road.highway',
        elementType: 'geometry',
        stylers: [
          {
            color: '#f8c967'
          }
        ]
      },
      {
        featureType: 'road.highway',
        elementType: 'geometry.stroke',
        stylers: [
          {
            color: '#e9bc62'
          }
        ]
      },
      {
        featureType: 'road.highway.controlled_access',
        elementType: 'geometry',
        stylers: [
          {
            color: '#e98d58'
          }
        ]
      },
      {
        featureType: 'road.highway.controlled_access',
        elementType: 'geometry.stroke',
        stylers: [
          {
            color: '#db8555'
          }
        ]
      },
      {
        featureType: 'road.local',
        elementType: 'labels.text.fill',
        stylers: [
          {
            color: '#806b63'
          }
        ]
      },
      {
        featureType: 'transit.line',
        elementType: 'geometry',
        stylers: [
          {
            color: '#dfd2ae'
          }
        ]
      },
      {
        featureType: 'transit.line',
        elementType: 'labels.text.fill',
        stylers: [
          {
            color: '#8f7d77'
          }
        ]
      },
      {
        featureType: 'transit.line',
        elementType: 'labels.text.stroke',
        stylers: [
          {
            color: '#ebe3cd'
          }
        ]
      },
      {
        featureType: 'transit.station',
        elementType: 'geometry',
        stylers: [
          {
            color: '#dfd2ae'
          }
        ]
      },
      {
        featureType: 'water',
        elementType: 'geometry.fill',
        stylers: [
          {
            color: '#b9d3c2'
          }
        ]
      },
      {
        featureType: 'water',
        elementType: 'labels.text.fill',
        stylers: [
          {
            color: '#92998d'
          }
        ]
      }
    ]
  })
}

export function useCreateInfoWindow(): google.maps.InfoWindow {
  return new google.maps.InfoWindow({
    content: '',
    disableAutoPan: true
  })
}

export const useItemMarker = () =>
  useState<IMarker[]>('itemMarker', () => {
    const itemPlan = [
      {
        id: 1,
        name: '',
        center: { lat: -31.56391, lng: 147.154312 },
        statusOpen: false,
        img: 'https://img.wongnai.com/p/256x256/2023/05/13/eab4588c1ca648faa8a5788fcd8c6b6d.jpg',
        time: ''
      },
      {
        id: 2,
        name: '',
        center: { lat: -33.718234, lng: 150.363181 },
        statusOpen: false,
        img: 'https://img.wongnai.com/p/256x256/2021/09/09/5ceb46b2f0454e43b30fd07ab1fce67b.jpg',
        time: ''
      },
      {
        id: 3,
        name: '',
        center: { lat: -33.727111, lng: 150.371124 },
        statusOpen: false,
        img: 'https://img.wongnai.com/p/256x256/2018/12/16/5f6f34108b3649819b34908c9b82f299.jpg',
        time: ''
      },
      {
        id: 4,
        name: '',
        center: { lat: -33.848588, lng: 151.209834 },
        statusOpen: false,
        img: 'https://img.wongnai.com/p/256x256/2022/11/17/ad05227347474e098e7f6390d410c1af.jpg',
        time: ''
      },
      {
        id: 5,
        name: '',
        center: { lat: -33.851702, lng: 151.216968 },
        statusOpen: false,
        img: 'https://img.wongnai.com/p/256x256/2019/09/25/5d081e9d4df147c5bd0178ce5cb3d3ca.jpg',
        time: ''
      },
      {
        id: 6,
        name: '',
        center: { lat: -34.671264, lng: 150.863657 },
        statusOpen: false,
        img: 'https://img.wongnai.com/p/256x256/2022/08/07/a08d6ca887ef44ff8faaf90ac810302e.jpg',
        time: ''
      },
      {
        id: 7,
        name: '',
        center: { lat: -35.304724, lng: 148.662905 },
        statusOpen: false,
        img: 'https://img.wongnai.com/p/256x256/2018/10/22/27bb72db88c4404eb4819fcce6ff4ca9.jpg',
        time: ''
      },
      {
        id: 8,
        name: '',
        center: { lat: -37.062834, lng: 142.748489 },
        statusOpen: false,
        img: 'https://img.wongnai.com/p/256x256/2018/11/07/605b202ac4da49688e5f212ae895ae8b.jpg',
        time: ''
      },
      {
        id: 9,
        name: '',
        center: { lat: -36.72236, lng: 142.1772 },
        statusOpen: false,
        img: 'https://t1.blockdit.com/photos/2023/05/6468f598518fcbde56ee815b_800x0xcover_VnzEfwHS.jpg',
        time: ''
      },
      {
        id: 10,
        name: '',
        center: { lat: -37.823873, lng: 140.76271 },
        statusOpen: false,
        img: 'https://img.wongnai.com/p/256x256/2021/06/23/83dcf5c337a64cf7b75467c276e982c3.jpg',
        time: ''
      }
    ]
    return itemPlan
  })
