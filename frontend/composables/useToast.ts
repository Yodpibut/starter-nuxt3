import { useToast } from 'vue-toastification'

export const useShowToast = () => {
  const toast: IToast = useToast()
  const showToast = (message: string, type: string) => toast[type](message)
  return showToast
}
