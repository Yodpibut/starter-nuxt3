export const useDrawer = () =>
  useState<boolean>('drawer', () => {
    const drawer = ref(true)
    const checkWindowWidth = () => {
      const w = window.innerWidth
      drawer.value = w < 1280 ? false : true
    }
    onMounted(() => {
      checkWindowWidth
      window.addEventListener('resize', checkWindowWidth)
    })
    return drawer
  })

// export const useRail = () =>
//   useState<boolean>('rail', () => {
//     const rail = ref(false)
//     return rail
//   })

export const useItemNav = () =>
  useState<INav[]>('itemNav', () => {
    const { locale, t } = useI18n()
    const $t = (key: any) => t(key, { locale })
    const itemNav = [
      {
        name: 'Map Group',
        path: '/',
        icon: 'mdi-map'
      },
      {
        name: 'Map Img Group',
        path: '/map-image-group',
        icon: 'mdi-map'
      },
      {
        name: 'Map Car Group',
        path: '/map-icon-group',
        icon: 'mdi-map'
      },
      {
        name: $t('nav.txUserList'),
        path: '/user-list',
        icon: 'mdi-forum'
      }
    ]
    return itemNav
  })
