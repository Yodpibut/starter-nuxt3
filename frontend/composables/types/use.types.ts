export interface INav {
  [key: string]: any
}

export interface IToast {
  [key: string]: any
}

export interface IMarker {
  [key: string]: any
}
