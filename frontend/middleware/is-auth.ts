export default defineNuxtRouteMiddleware(async (to, from) => {
  const authStore = useAuthStore()
  // console.log(authStore.accessToken )
  if (authStore.accessToken) {
    await authStore.getUser()
  }
  if (to.path === '/login' && authStore.accessToken) {
    return await navigateTo('/')
  }
  if (to.path !== '/login' && !authStore.accessToken) {
    return await navigateTo('/login')
  }
})
